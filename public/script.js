const button = document.querySelector('#button')
const result = document.querySelector('#result')


const getUuid = async () => {
    try {
        const res = await fetch('http://127.0.0.1/api/get-uuid')
        if (!res.ok) throw new Error('Result Failed')
        const data = await res.json()
        result.innerHTML = data.uuid
    } catch (err) {
        console.log(err)
    }

}


button.addEventListener('click', getUuid)